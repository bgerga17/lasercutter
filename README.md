
# README Laser Cutter

## Quick start
  1.   
    * Turn machine on and use the arrow keys on the machine to move the cutting head to the back right corner
    * Decide if you should use the knife bed or honnycomb bed 
    * REMEMBER TO ADJUST THE Z-HEIGHT TO THE CHOSEN CUTTING BED SO YOU DON'T RAM THE CUTTING HEAD AGAINST THE BED!
    * Put in your material (use only wood, acrylic or cardboard for beginners) 
    * Move the cutting head to somewhere smooth an even on your material
    * Use the autofocus feature to set the z-height.
  1.  
    * Use LightBurn software to handle your cut file. 
    * choose cut/engrave settings for each layer in LightBurn 
    * Press the send button in lightburn to send the file to the cutter
  1.  
    * press the file button the the lasercutter and choose your file (usually the first on the list) 
    * Use the arrow keys on the lasercutter to move the cutting head to where you want to cut 
    * press the frame button to see where the laser intends to cut 
    * Close the lid 
    * start ventilation, pump and laserpower
    * Press start on the lasercutter
  1.  
    * DO NOT LEAVE THE LASERCUTTER UNATTENDED. 
    STAY AND KEEP AN EYE THAT EVERYTHING IS OK THROUGHOUT THE WHOLE CUT.
    DO NOT OPEN THE LID IMMEDIATELY AFTER THE CUT HAS FINNISHED.
    * WAIT AT LEAST 15 SECONDS FOR THE FUMES TO BE SUCKED OUT
  1.  
    * Turn off laserpower, ventilation and pump and then finally the machine.

## Materials
Material |Cut params | Engrave params
---------|----------|-------------
Cardboard 2 layer | 120 mm/s  95% | 520mm/s 25%   -6mm z-offset
Cardboard 3 layer | 80 mm/s  95% | 520mm/s 25%   -6mm z-offset
---|---|---
MDF 6 mm | 12 mm/s | 95%	 
MDF 10 mm | 6 mm/s | 98%
---|---|---
PVC	| ABSOLUTLY NEVER EVER CUT PVC. IT PRODUCES TOXIC AND CORROSIVE GASSES THAT IS BAD FOR YOU AND THE MACHINE.|
---|---|---
 
## Laser
9 % seems to be the lowest level, under 9% the laser tube will not fire, it is supposedly rated 120 watts

## Machine
The machine itself is an EDUARD x1250 https://www.eduard.dk/eduart-x1250/ and is based around a Ruida 644XS controller (https://variometrum.hu/pdf/rdc6442g.pdf)

## Software
### Lightburn
https://lightburnsoftware.com/ 
Properly the most complete software for the laser cutter but does require an 80 USD license. It automatically detects the machine when plugged in and accepts all kinds of formats, SVG, DXF, AI, and even bitmaps. Runs on everything, Windows, macs and linux. The university has a license that is installed on the dedicated maker space PC next to the laser cutter. 
 
### Inkscape Plugin
On the cheap, we have https://github.com/jnweiger/inkscape-thunderlaser which worked somewhat out of the box on ubuntu 20.04 with Inkscape. It does have some rough edges, for instance, when clicking "Apply" the plugin starts cutting right away where normally you would expect it to just upload the workpiece to the cutter and then take it from there.
 
### Communication protocol
The machine does not use g-code but instead a proprietary "rd" format. There is not much information about the protocol online, but it is possible to find some reverse engineering projects on Github 
 
